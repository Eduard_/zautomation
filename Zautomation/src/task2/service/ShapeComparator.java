package task2.service;

public final class ShapeComparator {

  private ShapeComparator() {

  }

  /**
   * Check whether circle can be inserted into square;
   * 
   * @param squareArea
   * @param circleArea
   * @return
   */
  public static boolean isSquareCanAccomodateCircle(double squareArea, double circleArea) {
    // We need to calculate square side/circle diameter and compare them.

    double side = Math.sqrt(squareArea);
    double diam = Math.sqrt(circleArea / Math.PI) * 2;

    if (side < diam)
      return false;
    else
      return true;
  }

  /**
   * Check whether square can be inserted into circle;
   * 
   * @param squareArea
   * @param circleArea
   * @return
   */
  public static boolean isCircleCanAccomodateSquare(double squareArea, double circleArea) {
    // We need to calculate circle diam/square diagonal and compare them

    double diagonal = Math.sqrt(squareArea) * Math.sqrt(2);
    double diam = Math.sqrt(circleArea / Math.PI) * 2;

    if (diagonal < diam)
      return true;
    else
      return false;

  }

}
