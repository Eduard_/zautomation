package task2.app;

import java.text.NumberFormat;

import task1.model.Circle;
import task1.model.Square;
import task1.service.Randomizer;
import task2.service.ShapeComparator;

public class Application {

  public static NumberFormat formatter = task1.app.Application.formatter;

  public static void main(String[] args) {
    System.out.println("Task2");

    Square square = new Square(Randomizer.getRandomValue());
    System.out.println("Square area - " + formatter.format(square.getArea()));

    Circle circle = new Circle(Randomizer.getRandomValue());
    System.out.println("Circle area - " + formatter.format(circle.getArea()));

    if (ShapeComparator.isSquareCanAccomodateCircle(square.getArea(), circle.getArea()))
      System.out.println("Square can accomodate circle inside.");
    else
      System.out.println("Square can't accomodate circle inside.");

    if (ShapeComparator.isCircleCanAccomodateSquare(square.getArea(), circle.getArea()))
      System.out.println("Square can be placed inside circle.");
    else
      System.out.println("Square can't be placed inside circle.");

    System.out.println("Application end.");
  }
}
