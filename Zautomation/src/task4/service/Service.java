package task4.service;

public class Service {

  private Service() {

  }

  public static boolean isShitWord(String str) {
    if (str != null && !"".equals(str)) {
      StringBuffer buf = new StringBuffer(str);
      for (int i = 0; i < buf.length() / 2; i++)
        if (buf.charAt(i) != buf.charAt(buf.length() - 1 - i))
          return false;
    }
    return true;
  }

}
