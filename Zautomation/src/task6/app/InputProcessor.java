package task6.app;

public class InputProcessor {

  private InputProcessor() {
  }

  public static int processLengthInput() {
    String valueStr = null;
    int value = -1;
    for (int i = 0; i < 3; i++) {
      if (i != 0)
        System.out.println("Please try again. You have " + (3 - i) + " attempt(-s) left.");

      System.out.println("Please enter length value.");

      valueStr = System.console().readLine();

      try {
        value = Integer.parseInt(valueStr);
      } catch (NumberFormatException e) {
        continue;
      }

      if (value < 0) {
        continue;
      }

      break;
    }

    return value;

  }
}
