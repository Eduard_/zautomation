package task6.app;

import task6.model.Bulb;
import task6.model.ColorGarland;
import task6.model.SimpleGarland;

public class Application {

  public static void main(String[] args) {

    System.out.println("Task6");

    System.out.println("Please enter length for simple garland. Number must be greater then zero. ");
    int lenth = InputProcessor.processLengthInput();
    SimpleGarland garland = new SimpleGarland(lenth);

    System.out.println("\nCurrent state of simple garland is:");
    for (Bulb bulb : garland.getBulbs())
      System.out.println(bulb.toString());

    System.out.println("Please enter length for color garland. Number must be greater then zero. ");
    lenth = InputProcessor.processLengthInput();
    ColorGarland colorGarland = new ColorGarland(lenth);

    System.out.println("\nCurrent state of color garland is:");
    for (Bulb bulb : colorGarland.getBulbs())
      System.out.println(bulb.toString());

    System.out.println("Application end.");

  }

}
