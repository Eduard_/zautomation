package task6.model;

public enum BulbColor {

  RED("red"),

  YELLOW("yellow"),

  GREEN("green"),

  BLUE("blue");

  private String colorName;

  BulbColor(String colorName) {
    this.colorName = colorName;
  }

  @Override
  public String toString() {
    return colorName;
  }

  public static BulbColor getColorByCode(int code) {
    if (code % 4 == 0)
      return BulbColor.BLUE;
    else if (code % 3 == 0)
      return BulbColor.GREEN;
    else if (code % 2 == 0)
      return BulbColor.YELLOW;
    else
      return BulbColor.RED;
  }

}
