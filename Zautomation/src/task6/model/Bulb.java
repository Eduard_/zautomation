package task6.model;

public class Bulb {

  protected BulbState state;

  public BulbState getState() {
    return state;
  }

  public void setState(BulbState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "--*" + state.toString() + "*--";
  }

}
