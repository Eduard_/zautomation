package task6.model;

import java.util.Date;
import java.util.List;

public abstract class Garland {

  protected int bulbCounter;

  public Garland(int bulbCounter) {
    this.bulbCounter = bulbCounter;
  }

  public int getBulbCounter() {
    return bulbCounter;
  }

  public abstract List<Bulb> getBulbs();

  @SuppressWarnings("deprecation")
  protected BulbState setBulbStare(int bulbCounter) {
    Date date = new Date();
    boolean isEvenMinute = date.getMinutes() % 2 == 0;

    if ((bulbCounter % 2 == 0 && isEvenMinute) || (bulbCounter % 2 == 1 && !isEvenMinute))
      return BulbState.ON;
    else
      return BulbState.OFF;
  }
}
