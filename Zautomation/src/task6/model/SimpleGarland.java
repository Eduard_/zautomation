package task6.model;

import java.util.ArrayList;
import java.util.List;

public class SimpleGarland extends Garland {

  public SimpleGarland(int bulbCounter) {
    super(bulbCounter);
  }

  @Override
  public List<Bulb> getBulbs() {
    List<Bulb> list = new ArrayList<Bulb>();

    for (int i = 1; i <= bulbCounter; i++) {
      Bulb bulb = new Bulb();
      bulb.setState(setBulbStare(i));
      list.add(bulb);
    }

    return list;
  }

}
