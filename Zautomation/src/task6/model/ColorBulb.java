package task6.model;

public class ColorBulb extends Bulb {

  private BulbColor color = null;

  public ColorBulb(BulbColor color) {
    this.color = color;
  }

  public BulbColor getColor() {
    return color;
  }

  @Override
  public String toString() {
    if (BulbState.OFF.equals(state))
      return super.toString();
    else
      return "--*" + color.toString() + "*--";
  }

}
