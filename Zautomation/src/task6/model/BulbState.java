package task6.model;

public enum BulbState {

  ON("on"),

  OFF("off");

  private String state;

  BulbState(String state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return state;
  }

  public static BulbState getBulbStateByCode(int code) {
    if (code % 2 == 0)
      return BulbState.OFF;
    else
      return BulbState.ON;
  }
}
