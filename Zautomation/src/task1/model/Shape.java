package task1.model;

public abstract class Shape {

  protected double side = 0;

  public Shape(double side) {
    this.side = side;
  }

  public abstract double getArea();
}
