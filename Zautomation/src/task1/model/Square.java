package task1.model;

public class Square extends Shape {

  public Square(double side) {
    super(side);
  }

  @Override
  public double getArea() {
    return side * side;
  }

}
