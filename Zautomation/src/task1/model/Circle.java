package task1.model;

public class Circle extends Shape {

  public Circle(double side) {
    super(side);
  }

  @Override
  public double getArea() {
    return Math.PI * side * side / 4;
  }
}
