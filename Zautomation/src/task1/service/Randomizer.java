package task1.service;

public class Randomizer {

  private Randomizer() {
  }

  /**
   * Generates random in range [0.5; 3);
   * 
   * @return
   */
  public static double getRandomValue() {
    // generates from 0.0 to 3.0
    double value = Math.random() * 3;

    while (value < 0.5)
      value = Math.random() * 3;

    return value;
  }

}
