package task1.service;

public class Validator {

  private static final int MAX_VALUE = 100000;

  private Validator() {

  }

  public static boolean validate(String valueStr) {
    double value = 0;

    if (valueStr == null || valueStr.equals("")) {
      System.out.println("You've entered empty value.");
      return false;
    }
    try {
      value = Double.parseDouble(valueStr);
    } catch (NumberFormatException e) {
      System.out.println("You've entered wrong numeric value.");
      return false;
    }

    if (value != 0 && value < 0) {
      System.out.println("Value can't be negative");
      return false;
    }

    if (value > MAX_VALUE) {
      System.out.println("Value can't exceed " + MAX_VALUE);
      return false;
    }

    return true;
  }

}
