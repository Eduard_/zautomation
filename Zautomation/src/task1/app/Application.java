package task1.app;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import task1.model.Circle;
import task1.model.Square;

public class Application {

  public static NumberFormat formatter = new DecimalFormat("#0.00");

  public static void main(String[] args) {

    System.out.println("Task1");

    InputProcessor validator = new InputProcessor();
    double value = validator.takeConsoleInput();

    if (value != 0) {
      Square squareServ = new Square(value);
      double res = squareServ.getArea();
      System.out.println("\nSquare area - " + formatter.format(res));

      Circle circleServ = new Circle(value);
      res = circleServ.getArea();
      System.out.println("Circle area - " + formatter.format(res));

    }
    System.out.println("Application end.");
  }

}
