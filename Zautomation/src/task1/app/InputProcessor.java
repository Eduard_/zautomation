package task1.app;

import task1.service.Randomizer;
import task1.service.Validator;

public class InputProcessor {

  public double takeConsoleInput() {
    String valueStr = null;
    double value = 0;
    for (int i = 0; i < 3; i++) {
      if (i != 0)
        System.out.println("Please try again. You have " + (3 - i) + " attempt(-s) left.");

      System.out.println("Please enter square side or circle diameter.");

      valueStr = System.console().readLine();

      if (!Validator.validate(valueStr))
        continue;

    }

    if (value == 0) {
      System.out.println("Ok, we will take random values");
      value = Randomizer.getRandomValue();
      System.out.println("Random value - " + Application.formatter.format(value));
    }

    return value;
  }

}
