package task5.service;

public class Validator {

  private Validator() {

  }

  private static boolean validateInt(String valueStr) {
    try {
      Integer.parseInt(valueStr);
    } catch (NumberFormatException e) {
      return false;
    }
    return true;
  }

  public static boolean validatePriority(String valueStr) {
    int value = 0;

    if (!validateInt(valueStr))
      return false;

    value = Integer.parseInt(valueStr);

    if (value != 1 && value != 2 && value != 3)
      return false;

    return true;
  }

  public static boolean validateDureation(String valueStr) {
    int value = 0;

    if (!validateInt(valueStr))
      return false;

    value = Integer.parseInt(valueStr);

    if (value != 1 && value != 2 && value != 4)
      return false;

    return true;
  }

  public static boolean validateDays(String valueStr) {
    int value = 0;
    if (!validateInt(valueStr))
      return false;

    value = Integer.parseInt(valueStr);

    if (value <= 0)
      return false;

    return true;
  }

}
