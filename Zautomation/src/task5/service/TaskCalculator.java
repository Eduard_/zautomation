package task5.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import task5.model.Task;

public class TaskCalculator {

  private TaskCalculator() {

  }

  public static long calculateTotalDuration(List<Task> list) {
    long total = 0;

    for (Task task : list)
      total += task.getDuration();

    return total;
  }

  public static List<Task> getTaskWithPriority(List<Task> origList, int priority) {
    List<Task> priorityList = new ArrayList<Task>();

    for (Task task : origList)
      if (task.getPriority() == priority)
        priorityList.add(task);

    return priorityList;
  }

  /**
   * Return list of tasks that can be done for amount of days defined. Assumed that day is 8 hours and starts with high
   * priority.
   * 
   * @param list
   * @param days
   * @return
   */
  public static List<Task> calculateTaskForDays(List<Task> list, int days) {
    List<Task> resultList = new ArrayList<Task>();
    // We can either sort whole task list for priority from high to low
    // or create three separate list

    // I am lazy, so I simply sort existed list, with implementing Comparable for task-model.
    // reverse order - descending
    Collections.sort(list, Collections.reverseOrder());
    int maxPossibleHours = 8 * days;
    int resultHours = 0;

    for (Task task : list)
      if ((resultHours + task.getDuration()) < maxPossibleHours) {
        resultHours += task.getDuration();
        resultList.add(task);
      } else
        break;

    return resultList;
  }

}
