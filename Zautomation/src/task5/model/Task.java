package task5.model;

/**
 * Taks model class. Comparable by priority.
 * 
 * @author eduards
 *
 */
public class Task implements Comparable<Task> {

  private String name;

  private int priority = 0;

  private int duration = 0;

  public Task(String name, int priority, int duration) {
    this.name = name;
    this.priority = priority;
    this.duration = duration;
  }

  public String getName() {
    return name;
  }

  public int getPriority() {
    return priority;
  }

  public int getDuration() {
    return duration;
  }

  @Override
  public int compareTo(Task o) {
    return o.priority - this.priority;
  }

}
