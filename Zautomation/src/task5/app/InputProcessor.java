package task5.app;

import java.util.ArrayList;
import java.util.List;

import task5.model.Task;
import task5.service.Validator;

public class InputProcessor {

  private InputProcessor() {
  }

  public static List<Task> processInput() {
    List<Task> list = new ArrayList<Task>();

    System.out.println("Please enter all todo tasks. You can enter any amount of tasks you need.");
    System.out.println("To process to another step just enter empty task-name.");

    String taskName = null;

    for (; !"".equals(taskName);) {

      System.out.println("Please enter task-name:");
      taskName = System.console().readLine();
      if (taskName != null && !"".equals(taskName)) {
        list.add(processTaskInput(taskName));
      }
    }
    return list;
  }

  private static Task processTaskInput(String taskName) {
    Task task = null;
    int priority = 0;
    int duration = 0;

    // Priority part
    System.out.println("Now you need to enter task priority. 1 - highest and 3 - lowest one.");
    String value = null;

    priority = processPriorityInput();

    // Duration part
    System.out.println("Now you need to enter task duration in hours. Possible values are - 1, 2 or 4.");
    value = null;
    while (value == null || "".equals(value)) {
      System.out.println("Please enter task duration:");

      value = System.console().readLine();
      if (Validator.validateDureation(value)) {
        duration = Integer.parseInt(value);
        break;
      } else {
        System.out.println("You've entered wrong number.");
        value = null;
      }
    }

    if (priority != 0 && duration != 0)
      task = new Task(taskName, priority, duration);
    return task;

  }

  public static int processPriorityInput() {
    int priority = 0;

    String value = null;
    while (value == null || "".equals(value)) {
      System.out.println("Please enter priority:");

      value = System.console().readLine();
      if (Validator.validatePriority(value)) {
        priority = Integer.parseInt(value);
        break;
      } else {
        System.out.println("You've entered wrong number.");
        value = null;
      }
    }

    return priority;
  }

  public static int processDaysInput() {
    int days = 0;

    String value = null;
    while (value == null || "".equals(value)) {
      System.out.println("Please enter number of days:");

      value = System.console().readLine();
      if (Validator.validateDays(value)) {
        days = Integer.parseInt(value);
        break;
      } else {
        System.out.println("You've entered wrong number.");
        value = null;
      }
    }

    return days;

  }
}
