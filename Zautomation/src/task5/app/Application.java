package task5.app;

import java.util.List;

import task5.model.Task;
import task5.service.TaskCalculator;

public class Application {

  public static void main(String[] args) {

    System.out.println("Task5");

    List<Task> list = InputProcessor.processInput();
    System.out.println("\nFor all your tasks you need " + TaskCalculator.calculateTotalDuration(list) + " hours.");

    System.out.println("Now you need to enter priority to see what task you have with one.");
    int priority = InputProcessor.processPriorityInput();
    for (Task task : TaskCalculator.getTaskWithPriority(list, priority))
      printTask2Console(task);

    System.out.println("Now you need to enter amount of days to get list of task that can be done.");
    int days = InputProcessor.processDaysInput();
    for (Task task : TaskCalculator.calculateTaskForDays(list, days))
      printTask2Console(task);

    System.out.println("Application end.");
  }

  private static void printTask2Console(Task task) {
    System.out.println("\nName:\t" + task.getName());
    System.out.println("Duration:\t" + task.getDuration());
    System.out.println("Priority:\t" + task.getPriority());
  }
}
