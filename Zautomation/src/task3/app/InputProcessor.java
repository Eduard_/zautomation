package task3.app;

import task3.app.service.DigitValidator;

public class InputProcessor {

  public static long takeConsoleInput() {
    String valueStr = null;
    long value = -1;
    for (int i = 0; i < 3; i++) {
      if (i != 0)
        System.out.println("Please try again. You have " + (3 - i) + " attempt(-s) left.");

      System.out.println("Please enter range value.");

      valueStr = System.console().readLine();

      if (!DigitValidator.validateRangeDigit(valueStr))
        continue;
      else {
        value = Long.parseLong(valueStr);
        break;
      }

    }

    return value;
  }

}
