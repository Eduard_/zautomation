package task3.app.service;

import java.util.ArrayList;
import java.util.List;

public class Service {

  private Service() {

  }

  public static long calculate(long v1, long v2) {
    long result = 0;
    List<Long> arrayList = new ArrayList<Long>();
    for (long l = v1; l <= v2; l++)
      arrayList.add(Long.valueOf(l));

    for (Long l : arrayList)
      if (l % 3 == 0 && l % 5 != 0)
        result += l;

    return result;
  }
}
