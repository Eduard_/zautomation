package task3.app.service;

public class DigitValidator {

  public static boolean validateRangeDigit(String valueStr) {
    long value = -1;
    try {
      value = Long.parseLong(valueStr);
    } catch (NumberFormatException e) {
      return false;
    }

    if (value < 0) {
      return false;
    }

    return true;
  }

  public static boolean validateRange(long v1, long v2) {
    if (v1 > v2)
      return false;
    return true;
  }
}
