package task3.app;

import task3.app.service.DigitValidator;
import task3.app.service.Service;

public class Application {

  public static void main(String[] args) {
    // TODO Auto-generated method stub

    System.out.println("Task3");
    System.out.println("You need to input range of natural numbers");
    System.out.println("Please enter first part of range");

    long firstValue = InputProcessor.takeConsoleInput();
    if (-1 == firstValue)
      return;
    else {
      long secondValue = InputProcessor.takeConsoleInput();
      if (-1 != secondValue && DigitValidator.validateRange(firstValue, secondValue)) {
        long value = Service.calculate(firstValue, secondValue);

        System.out.println("Total sum of numbers that can be devided by 3 and can't be devided by 5 is - " + value);
      }
    }
  }

}
